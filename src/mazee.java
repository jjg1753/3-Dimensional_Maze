
/* maze.java
*
* Version   $Id: JavaRunTimeVersion.java,v 1.8 2017/09/10 18:14:49 $
*
*/


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


/**
 * This program finds a path through a 3D maze if path exists.
 *
 * @author Jeet Gandhi
 * @author Brij Shah
 */
public class mazee {


    public static void main(String[] args) {
        String findLengthMaze = "";//Used to find length of maze
        int breadth = 0, depth = 1, length = 0;//Used to get the length breadth and depth of the matrix
        String entireFile = "";//Used to store the entire file as a string
        String[][][] mazeArray = new String[30][30][30];//Used to store the maze in a 3d array
        try {
            Scanner readFile = new Scanner(new File("pattern")); //Reading the file
            findLengthMaze = readFile.nextLine();
            while (readFile.hasNext()) { //To calculate the breadth and depth of the maze
                if (readFile.nextLine().equals("next level")) {
                    depth++;
                } else {
                    breadth++;
                }
            }
            breadth = (breadth + 1) / depth; //Finding breadth of single level
            length = findLengthMaze.length();
            readFile.close();
        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
        }
        try {
            Scanner readFile = new Scanner(new File("pattern"));
            while (readFile.hasNext()) {
                entireFile = entireFile.concat(readFile.nextLine());//Storing entire pattern file into a string
            }
            readFile.close();
        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
        }
        char[] entireFileToChar = entireFile.toCharArray();

        findMaze(initializeMaze(length, breadth, entireFileToChar), length, breadth, depth);
    }

    /**
     * Storing the entire maze into a 3d array
     *
     * @param length           length of maze
     * @param breadth          breadth of maze
     * @param entireFileToChar maze stored in string
     * @return ed array containing the maze structure
     */
    private static String[][][] initializeMaze(int length, int breadth, char[] entireFileToChar) {
        String[][][] mazeArray = new String[30][30][30];
        for (int l = 0, i = 0, j = 0, k = 0; l < entireFileToChar.length; l++) { //Inserting the pattern into 3d maze
            if (String.valueOf(entireFileToChar[l]).equals("#") || String.valueOf(entireFileToChar[l]).equals(".")) {
                if ((i) % length == 0 && i != 0) {
                    i = 0;
                    j++;
                }
                if ((j) % breadth == 0 && j != 0) {
                    j = 0;
                    k = k + 1;
                }
                mazeArray[i][j][k] = String.valueOf(entireFileToChar[l]);
                i++;
            }
        }
        return mazeArray;
    }

    /**
     * This function finds a path through maze if it exists
     *
     * @param mazeArray array containing maze structure
     * @param l         length of maze
     * @param b         breadth of maze
     * @param d         depth of maze
     */
    public static void findMaze(String[][][] mazeArray, int l, int b, int d) {

        System.out.println("Enter x coordinate of starting point");
        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
        System.out.println("Enter y coordinate of starting point");
        int j = sc.nextInt();
        System.out.println("Enter z coordinate of starting point");
        int k = sc.nextInt();
        int count = 0;
        /* Flag for different directions */
        int forwardFlag = 0;
        int backWardFlag = 0;
        int leftFlag = 0;
        int rightFlag = 0;
        int upFlag = 0;
        int downFlag = 0;
        /* To follow the starting index through the maze */
        int start_i = i;
        int start_j = j;
        int start_k = k;
        int[][][] decisions = new int[30][30][30];// Storing the blocked decisions
        String[][][] mazeArrayCopy = new String[30][30][30]; // To reset the maze array
        for (int z = 0; z < d; z++) {
            for (int y = 0; y < b; y++) {
                for (int x = 0; x < l; x++) {
                    mazeArrayCopy[x][y][z] = mazeArray[x][y][z]; //making a copy of the maze so as to reset array
                }
            }
        }

        //Ensuring entry point is only at . and corners
        if ((i > 0 && j > 0 && i != l - 1 && j != b - 1) || (i < l - 1) && j < j - 1 || mazeArray[i][j][k].equals("#")) {
            System.out.println("Invalid Entry Point");
            System.exit(0);
        } else {
            do {
                //Checking if you can move in any direction or not
                if (!((j != forward(i, j, k, mazeArray, b) && forwardFlag == 0 && decisions[i][j + 1][k] == 0) ||
                        (i != right(i, j, k, mazeArray, l) && rightFlag == 0 && decisions[i + 1][j][k] == 0) ||
                        (j != backward(i, j, k, mazeArray) && backWardFlag == 0 && decisions[i][j - 1][k] == 0) ||
                        (i != left(i, j, k, mazeArray) && leftFlag == 0 && decisions[i - 1][j][k] == 0) ||
                        (k != down(i, j, k, mazeArray, d) && downFlag == 0 && decisions[i][j][k + 1] == 0) ||
                        (k != up(i, j, k, mazeArray) && upFlag == 0 && decisions[i][j + 1][k - 1] == 0))) {
                    decisions[i][j][k] = 1;
                    count = count + 1;
                    for (int z = 0; z < d; z++) {
                        for (int y = 0; y < b; y++) {
                            for (int x = 0; x < l; x++) {
                                mazeArray[x][y][z] = mazeArrayCopy[x][y][z];
                            }
                        }
                    }
                    System.out.println("RESET");
                    i = start_i;
                    j = start_j;
                    k = start_k;
                    forwardFlag = 0;
                    backWardFlag = 0;
                    leftFlag = 0;
                    rightFlag = 0;
                    upFlag = 0;
                    downFlag = 0;
                }
                //checking if you can move forward
                if (j != forward(i, j, k, mazeArray, b) && forwardFlag == 0) {
                    if (j + 1 < b) {
                        if (decisions[i][j + 1][k] == 0) {
                            count = 0;
                            backWardFlag = 1;
                            forwardFlag = 0;
                            upFlag = 0;
                            downFlag = 0;
                            rightFlag = 0;
                            leftFlag = 0;
                            System.out.println("went forward");
                            // System.out.println("Decision of"+i+" "+j+" "+k+"="+decisions[i][j][k]);
                            j = forward(i, j, k, mazeArray, b);
                            mazeArray[i][j][k] = "~";
                            if ((i == start_i && j == start_j && k == start_k)) {
                                decisions[i][j][k] = 0;
                            }
                            continue;
                        }
                    }
                }
                //checking if you can move right
                if (i != right(i, j, k, mazeArray, l) && rightFlag == 0) {
                    if (i + 1 < l) {
                        if (decisions[i + 1][j][k] == 0) {
                            count = 0;
                            backWardFlag = 0;
                            forwardFlag = 0;
                            upFlag = 0;
                            downFlag = 0;
                            rightFlag = 0;
                            leftFlag = 1;
                            System.out.println("went right");

                            //System.out.println("Decision of"+i+" "+j+" "+k+"="+decisions[i][j][k]);
                            i = right(i, j, k, mazeArray, l);
                            mazeArray[i][j][k] = "~";
                            if ((i == start_i && j == start_j && k == start_k)) {
                                decisions[i][j][k] = 0;
                            }
                            continue;
                        }
                    }

                }
                //checking if you can move left
                if (i != left(i, j, k, mazeArray) && leftFlag == 0) {
                    if (i - 1 >= 0) {
                        if (decisions[i - 1][j][k] == 0) {
                            count = 0;
                            backWardFlag = 0;
                            forwardFlag = 0;
                            upFlag = 0;
                            downFlag = 0;
                            rightFlag = 1;
                            leftFlag = 0;
                            System.out.println("went left");

                            //System.out.println("Decision of"+i+" "+j+" "+k+"="+decisions[i][j][k]);
                            i = left(i, j, k, mazeArray);
                            mazeArray[i][j][k] = "~";
                            if ((i == start_i && j == start_j && k == start_k)) {
                                decisions[i][j][k] = 0;
                            }
                            continue;
                        }
                    }
                }
                //checking if you can move backward
                if (j != backward(i, j, k, mazeArray) && backWardFlag == 0) {
                    if (j - 1 >= 0) {
                        if (decisions[i][j - 1][k] == 0) {
                            count = 0;
                            backWardFlag = 0;
                            forwardFlag = 1;
                            upFlag = 0;
                            downFlag = 0;
                            rightFlag = 0;
                            leftFlag = 0;
                            System.out.println("went backward");
                            // System.out.println("Decision of"+i+" "+j+" "+k+"="+decisions[i][j][k]);
                            j = backward(i, j, k, mazeArray);
                            mazeArray[i][j][k] = "~";
                            if ((i == start_i && j == start_j && k == start_k)) {
                                decisions[i][j][k] = 0;
                                //  System.out.println("Decision reset");
                            }
                            continue;
                        }
                    }
                }
                //checking if you can move down
                if (k != down(i, j, k, mazeArray, d) && downFlag == 0) {
                    if (k + 1 < d) {
                        if (decisions[i][j][k + 1] == 0) {
                            count = 0;
                            backWardFlag = 0;
                            forwardFlag = 0;
                            upFlag = 1;
                            downFlag = 0;
                            rightFlag = 0;
                            leftFlag = 0;
                            System.out.println("went down");
                            //System.out.println("Decision of" + i + " " + j + " " + k + "=" + decisions[i][j][k]);
                            k = down(i, j, k, mazeArray, d);
                            mazeArray[i][j][k] = "~";
                            if ((i == start_i && j == start_j && k == start_k)) {
                                decisions[i][j][k] = 0;
                            }
                            continue;
                        }
                    }
                }
                //checking if you can move up
                if (k != up(i, j, k, mazeArray) && upFlag == 0) {
                    if (k - 1 >= 0) {
                        if (decisions[i][j][k - 1] == 0) {
                            count = 0;
                            backWardFlag = 1;
                            forwardFlag = 0;
                            upFlag = 0;
                            downFlag = 0;
                            rightFlag = 0;
                            leftFlag = 0;
                            System.out.println("went up");
                            //System.out.println("Decision of" + i + " " + j + " " + k + "=" + decisions[i][j][k]);
                            k = up(i, j, k, mazeArray);
                            mazeArray[i][j][k] = "~";
                            if ((i == start_i && j == start_j && k == start_k)) {
                                decisions[i][j][k] = 0;
                            }
                            continue;
                        }
                    }


                }


            } while (i > 0 && i < l - 1 && j > 0 && j < b - 1 && count < 3);
        }
        //checking if there is a path or not
        if ((j == b - 1 || i == l - 1 || i == 0 || j == 0) && !(i == start_i && j == start_j && k == start_k)) {
            mazeArray[start_i][start_j][start_k] = "~";
            System.out.println("path found");
            for (int z = 0; z < d; z++) {
                for (int y = 0; y < b; y++) {
                    for (int x = 0; x < l; x++) {
                        System.out.print(mazeArray[x][y][z]);
                    }
                    System.out.println("");
                }
                System.out.println("");
            }
        } else {
            System.out.println("Not found");
            System.exit(0);
        }
    }

    /**
     * This function checks if you can move left
     *
     * @param i         current position of x axis
     * @param j         current position of y axis
     * @param k         current position of z axis
     * @param mazeArray 3d array containing maze
     * @return value of i
     */
    public static int left(int i, int j, int k, String[][][] mazeArray) {
        if (i != 0)
            if (mazeArray[i - 1][j][k].equals("."))
                i = i - 1;
        return i;
    }

    /**
     * This function checks if you can move right
     *
     * @param i         current position of x axis
     * @param j         current position of y axis
     * @param k         current position of z axis
     * @param mazeArray 3d array containing maze
     * @return value of i
     */
    public static int right(int i, int j, int k, String[][][] mazeArray, int l) {
        if (i != l - 1)
            if (mazeArray[i + 1][j][k].equals("."))
                i = i + 1;
        return i;
    }

    /**
     * This function checks if you can move forward
     *
     * @param i         current position of x axis
     * @param j         current position of y axis
     * @param k         current position of z axis
     * @param mazeArray 3d array containing maze
     * @return value of j
     */
    public static int forward(int i, int j, int k, String[][][] mazeArray, int b) {
        if (j != b - 1)
            if (mazeArray[i][j + 1][k].equals("."))
                j = j + 1;
        return j;
    }

    /**
     * This function checks if you can move backward
     *
     * @param i         current position of x axis
     * @param j         current position of y axis
     * @param k         current position of z axis
     * @param mazeArray 3d array containing maze
     * @return value of j
     */
    public static int backward(int i, int j, int k, String[][][] mazeArray) {
        if (j != 0)
            if (mazeArray[i][j - 1][k].equals("."))
                j = j - 1;
        return j;
    }

    /**
     * This function checks if you can move up
     *
     * @param i         current position of x axis
     * @param j         current position of y axis
     * @param k         current position of z axis
     * @param mazeArray 3d array containing maze
     * @return value of k
     */
    public static int up(int i, int j, int k, String[][][] mazeArray) {
        if (k != 0)
            if (mazeArray[i][j][k - 1].equals("."))
                k = k - 1;
        return k;
    }

    /**
     * This function checks if you can move down
     *
     * @param i         current position of x axis
     * @param j         current position of y axis
     * @param k         current position of z axis
     * @param mazeArray 3d array containing maze
     * @return value of k
     */
    public static int down(int i, int j, int k, String[][][] mazeArray, int d) {
        if (k != d - 1)
            if (mazeArray[i][j][k + 1].equals("."))
                k = k + 1;
        return k;
    }
}

